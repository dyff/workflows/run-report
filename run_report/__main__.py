# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
import os
import subprocess
from pathlib import Path

import absl.app
import absl.flags
import ruamel.yaml
from absl import logging
from dyff.audit.analysis import AnalysisContext, Scores, legacy_run_report, run_analysis
from dyff.schema.platform import ScoreMetadataRefs
from ruamel.yaml.compat import StringIO as YAMLStringIO

# -----------------------------------------------------------------------------

FLAGS = absl.flags.FLAGS

# "Report mode" flags
# TODO: Report mode is deprecated; remove these flags once it's removed.

absl.flags.DEFINE_string(
    "rubric", None, "Qualified Python module name of the Rubric to apply"
)

absl.flags.DEFINE_string("dataset_path", None, "Local path to the Dataset data")

absl.flags.DEFINE_string("evaluation_path", None, "Local path to the Evaluation data")

absl.flags.DEFINE_string(
    "output_path", None, "Local path to where the report output should go"
)

absl.flags.DEFINE_multi_string(
    "module", None, "Local python module directory to append to sys.path"
)

# -----------------------------------------------------------------------------


def main(_unused_argv) -> None:
    logging.set_verbosity(logging.DEBUG)

    if FLAGS.rubric:
        logging.warning("Running as Report (deprecated)")

        try:
            legacy_run_report(
                rubric=FLAGS.rubric,
                dataset_path=FLAGS.dataset_path,
                evaluation_path=FLAGS.evaluation_path,
                output_path=FLAGS.output_path,
                modules=FLAGS.module,
            )
        except subprocess.CalledProcessError as ex:
            print(ex.stdout)
            print(ex.stderr)
            raise
    else:
        logging.info("Running as Analysis")

        config_file = os.environ.get("DYFF_AUDIT_ANALYSIS_CONFIG_FILE")
        if config_file is None:
            raise ValueError(
                "environment variable DYFF_AUDIT_ANALYSIS_CONFIG_FILE is required"
            )

        local_storage_root = os.environ.get("DYFF_AUDIT_LOCAL_STORAGE_ROOT")
        if local_storage_root is None:
            raise ValueError(
                "environment variable DYFF_AUDIT_LOCAL_STORAGE_ROOT is required"
            )

        yaml = ruamel.yaml.YAML()
        with open(config_file, "r") as fin:
            analysis_yaml = yaml.load(fin)
        yaml_string = YAMLStringIO()
        yaml.dump(analysis_yaml, yaml_string)
        logging.info(f"analysis_yaml:\n{yaml_string.getvalue()}")

        try:
            ctx = AnalysisContext()
            run_analysis(
                ctx.analysis.method,
                storage_root=Path(local_storage_root),
                config_file=Path(config_file),
            )
        except subprocess.CalledProcessError as ex:
            print(ex.stdout)
            print(ex.stderr)
            raise

        # SECURITY: Analysis code can change values in the scores.json file,
        # since it has to be writeable by the analysis notebook. By changing
        # the .analysis field, malicious analysis code could overwrite scores
        # from an unrelated analysis in the database, or cause scores from the
        # malicious analysis to be incorrectly associated with the wrong
        # entities.
        # To avoid this vulnerability, we need to set the sensitive fields
        # here, in code that we control. This includes all fields that are
        # "references" to other entities.
        scores_file_path = Path(local_storage_root) / ctx.id / ".dyff" / "scores.json"
        try:
            scores_data: Scores = Scores.parse_file(scores_file_path)
        except FileNotFoundError:
            # No scores.json file, so nothing to do
            return

        for score in scores_data.scores:
            # SECURITY: All "reference" fields are potentially sensitive since
            # they control data display and aggregation behavior.
            # Note that AnalysisContext captures the configuration in __init__,
            # and we construct the context before invoking untrusted code, so
            # these member values will be correct even if untrusted code
            # modifies the config file.
            score.analysis = ctx.id
            score.metadata.refs = ScoreMetadataRefs(
                method=ctx.analysis.method.id,
                **ctx.analysis.scope.dict(),
            )
        with open(scores_file_path, "w") as fout:
            fout.write(scores_data.json())


if __name__ == "__main__":
    absl.app.run(main)
